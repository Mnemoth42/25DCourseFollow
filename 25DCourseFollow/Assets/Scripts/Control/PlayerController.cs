﻿using System;
using UnityEngine;

namespace JRPG.Control
{
    public class PlayerController : MonoBehaviour
    {
        private PlayerControls playerControls;

        private void Awake()
        {
            playerControls = new PlayerControls();
        }

        private void OnEnable()
        {
            playerControls.Enable();
        }

        private void OnDisable()
        {
            playerControls.Disable();
        }

        private void OnDestroy()
        {
            playerControls.Dispose();
        }
    }
}